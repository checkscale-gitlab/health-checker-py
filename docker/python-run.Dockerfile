FROM python:3.8

RUN pip install pyyaml requests twisted

CMD ["python", "/health-checker/main.py", "/config.yml", "/template.html", "/output.html", "600"]
