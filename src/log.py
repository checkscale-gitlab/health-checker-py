from enum import Enum
from datetime import datetime


class LogLevel(Enum):
    INFO = 1
    DEBUG = 2
    WARN = 3
    ERROR = 4


def log(log_level, message, indent=0):
    date_str = datetime.now()
    idents = ''.join(['\t' for i in range(0, indent)])
    print(f"[{log_level.name}] {date_str} - {idents}{message}")
